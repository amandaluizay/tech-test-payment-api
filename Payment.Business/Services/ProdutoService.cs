﻿using Payment.Business.Interfaces;
using Payment.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Business.Services
{
    public class ProdutoService : BaseService, IProdutoService
    {
        private readonly IVendaRepository _Repository;
        private readonly IVendedorRepository _vendedorRepository;
        private readonly IProdutoRepository _produtoRepository;


        public ProdutoService(IVendaRepository Repository,
                            IVendedorRepository VendedorRepository,
                            IProdutoRepository ProdutoRepository,
                            INotificador notificador) : base(notificador)
        {
            _Repository = Repository;
            _vendedorRepository = VendedorRepository;
            _produtoRepository = ProdutoRepository;
        }

        public async Task Adicionar(Produto produto)
        {
            await _produtoRepository.Adicionar(produto);
        }

        public async Task Atualizar(Produto produto)
        {
            await _produtoRepository.Atualizar(produto);
        }

        public async Task Remover(Guid id)
        {

            await _Repository.Remover(id);
        }

        public void Dispose()
        {
            _Repository?.Dispose();

        }
    }
}