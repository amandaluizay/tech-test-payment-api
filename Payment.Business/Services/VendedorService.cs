﻿using Payment.Business.Interfaces;
using Payment.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Business.Services
{
    public class VendedorService : BaseService, IVendedorService
    {
        private readonly IVendedorRepository vendedorRepository;

        public VendedorService(IVendedorRepository _vendedorRepository, INotificador notificador) : base(notificador)
        {
            vendedorRepository = _vendedorRepository;
        }

        public async Task Adicionar(Vendedor vendedor)
        {
            await vendedorRepository.Adicionar(vendedor);
        }

        public async Task Atualizar(Vendedor vendedor)
        {
            await vendedorRepository.Atualizar(vendedor);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public async Task Remover(Guid id)
        {
            await vendedorRepository.Remover(id);
        }
    }
}
