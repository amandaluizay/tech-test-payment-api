﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Business.Models
{
    public class Venda : Entity
    {
        public Status Status { get; set; }
        public Guid ProdutoId { get; set; }
        public Guid VendedorId { get; set; }
        // EF
        public Vendedor Vendedor { get; set; }
        public Produto Produto { get; set; }

        public Venda()
        {
            Status = Status.Aguardando_pagamento;
        }
    }
}
