﻿namespace Payment.Business.Models
{
    public class Produto: Entity
    {
        public string Descricao { get; set; }
        public int Preco { get; set; }

        public IEnumerable<Venda> Vendas { get; set;}
    }
}