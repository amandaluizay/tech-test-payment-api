﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Business.Models
{
    public enum Status
    {
        Aguardando_pagamento = 1,
        Pagamento_aprovado,
        Enviado_para_transportadora,
        Entregue,
        Cancelada


    }
}
