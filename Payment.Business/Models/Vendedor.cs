﻿namespace Payment.Business.Models
{
    public class Vendedor : Entity
    {
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Email { get; set; }
        public int Telefone { get; set; }
        public IEnumerable<Venda> Vendas { get; set; }
    }
}