﻿using Payment.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Business.Interfaces
{
    public interface IVendedorService : IDisposable
    {
        Task Adicionar(Vendedor vendedor);
        Task Atualizar(Vendedor vendedor);
        Task Remover(Guid id);
    }
}
