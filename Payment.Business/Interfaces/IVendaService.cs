﻿using Payment.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Business.Interfaces
{
    public interface IVendaService : IDisposable
    {
        Task Adicionar(Venda produto);
        Task Atualizar(Venda produto);
        Task Remover(Guid id);
    }
}
