﻿using Microsoft.EntityFrameworkCore;
using Payment.Business.Interfaces;
using Payment.Business.Models;
using Payment.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Data.Repository
{
    public class VendaRepository : Repository<Venda>, IVendaRepository
    {
        public VendaRepository(MeuDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Venda>> ObterVendasPorVendedor(Guid id)
        {
            return (IEnumerable<Venda>)await Db.Vendas.AsNoTracking().Include(f => f.Vendedor)
                .FirstOrDefaultAsync(p => p.Id == id);
        }


        public async Task<Produto> ObterVendaVendedor(Guid id)
        {
            return (Produto)await Buscar(p => p.VendedorId == id);
        }
    }
}
