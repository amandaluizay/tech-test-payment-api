﻿namespace Payment.APi.ViewModels
{
    public enum StatusViewModel
    {
        Aguardando_pagamento = 1,
        Pagamento_aprovado,
        Enviado_para_transportadora,
        Entregue,
        Cancelada
    }
}