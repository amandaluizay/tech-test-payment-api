﻿using System.ComponentModel.DataAnnotations;

namespace Payment.APi.ViewModels
{
    public class VendedorViewModel
    {
        [Key]
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Email { get; set; }
        public int Telefone { get; set; }
        public IEnumerable<VendaViewModel> Vendas { get; set; }
    }
}