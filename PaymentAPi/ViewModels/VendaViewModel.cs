﻿using System.ComponentModel.DataAnnotations;

namespace Payment.APi.ViewModels
{
    public class VendaViewModel
    {
        [Key]
        public Guid Id { get; set; }
        public StatusViewModel Status { get; set; }
        public Guid ProdutoId { get; set; }
        public Guid VendedorId { get; set; }
        // EF
        public VendedorViewModel Vendedor { get; set; }
        public ProdutoViewModel Produto { get; set; }

        public VendaViewModel()
        {
            Status = StatusViewModel.Aguardando_pagamento;
        }
    }
}