﻿using System.ComponentModel.DataAnnotations;

namespace Payment.APi.ViewModels
{
    public class ProdutoViewModel
    {
        [Key]
        public Guid Id { get; set; }    
        public string Descricao { get; set; }
        public int Preco { get; set; }

        public IEnumerable<VendaViewModel> Vendas { get; set; }
    }
}
