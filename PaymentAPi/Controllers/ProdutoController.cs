﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Payment.APi.ViewModels;
using Payment.Business.Interfaces;
using Payment.Business.Models;
using Payment.Data.Repository;

namespace Payment.APi.Controllers
{

    [Route("api/Produtos")]
    public class ProdutoController : MainController
    {
        private readonly IProdutoRepository _produtoRepository;
        private readonly IProdutoService _produtoService;
        private readonly IMapper _mapper;

        public ProdutoController(IProdutoService produtoService, IProdutoRepository produtoRepository, INotificador notificador, IMapper mapper) : base(notificador)
        {
             _mapper = mapper;
            _produtoRepository = produtoRepository;
            _produtoService = produtoService;
        }

        [HttpGet]
        public async Task<IEnumerable<ProdutoViewModel>> ObterTodos()
        {
            var produto = _mapper.Map<IEnumerable<ProdutoViewModel>>(await _produtoRepository.ObterTodos());
            return produto;
        }


        [HttpGet("{id:guid}")]
        // [Authorize]
        public async Task<ActionResult<ProdutoViewModel>> ObterpPorId(Guid id)
        {
            var veiculo = _mapper.Map<ProdutoViewModel>(await _produtoRepository.ObterPorId(id));

            if (veiculo == null) return NotFound();

            return veiculo;
        }


        [HttpPost]
        public async Task<ActionResult<ProdutoViewModel>> Adicionar(ProdutoViewModel produto)
        {
            if (!ModelState.IsValid) return CostumResponse(ModelState);

            await _produtoRepository.Adicionar(_mapper.Map<Produto>(produto));

            return CostumResponse(produto);
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<ProdutoViewModel>> Atualizar(Guid id, ProdutoViewModel produto)
        {
            if (id != produto.Id)
            {
                NotificarErro("O id informado não é o mesmo que foi passado na query");
                return CostumResponse(produto);
            }

            if (!ModelState.IsValid) return CostumResponse(ModelState);

            await _produtoService.Atualizar(_mapper.Map<Produto>(produto));

            return CostumResponse(produto);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<ProdutoViewModel>> Excluir(Guid id)
        {
            var categoriaViewModel = await _produtoRepository.ObterPorId(id);


            if (categoriaViewModel == null) return NotFound();

            await _produtoRepository.Remover(id);

            return CostumResponse(categoriaViewModel);
        }

        
    }
}

