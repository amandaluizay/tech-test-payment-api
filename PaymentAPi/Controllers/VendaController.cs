﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Payment.APi.ViewModels;
using Payment.Business.Interfaces;
using Payment.Business.Models;
using Payment.Data.Repository;

namespace Payment.APi.Controllers
{

    [Route("api/Vendas")]
    public class VendaController : MainController
    {
        private readonly IMapper _mapper;
        private readonly IVendaRepository _vendaRepository;
        private readonly IVendaService _vendaService;

        public VendaController(IVendaService service, IVendaRepository repository, IMapper mapper, INotificador notificador) : base(notificador)
        {
            _mapper = mapper;
            _vendaRepository = repository;
            _vendaService = service;
        }

        [HttpGet]
        public async Task<IEnumerable<VendaViewModel>> ObterTodos()
        {
            var venda = _mapper.Map<IEnumerable<VendaViewModel>>(await _vendaRepository.ObterTodos());
            return venda;
        }


        [HttpGet("{id:guid}")]
        // [Authorize]
        public async Task<ActionResult<VendaViewModel>> ObterpPorId(Guid id)
        {
            var veiculo = _mapper.Map<VendaViewModel>(await _vendaRepository.ObterPorId(id));

            if (veiculo == null) return NotFound();

            return veiculo;
        }


        [HttpPost]
        public async Task<ActionResult<VendaViewModel>> Adicionar(VendaViewModel venda)
        {
            if (!ModelState.IsValid) return CostumResponse(ModelState);

            await _vendaRepository.Adicionar(_mapper.Map<Venda>(venda));

            return CostumResponse(venda);
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<VendaViewModel>> Atualizar(Guid id, VendaViewModel venda)
        {
            if (id != venda.Id)
            {
                NotificarErro("O id informado não é o mesmo que foi passado na query");
                return CostumResponse(venda);
            }

            if (!ModelState.IsValid) return CostumResponse(ModelState);

            await _vendaService.Atualizar(_mapper.Map<Venda>(venda));

            return CostumResponse(venda);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Venda>> Excluir(Guid id)
        {
            var venda = await _vendaRepository.ObterPorId(id);


            if (venda == null) return NotFound();

            await _vendaRepository.Remover(id);

            return CostumResponse(venda);
        }

        
    }
}

