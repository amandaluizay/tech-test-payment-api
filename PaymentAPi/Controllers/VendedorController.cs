﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Payment.APi.ViewModels;
using Payment.Business.Interfaces;
using Payment.Business.Models;
using Payment.Data.Repository;

namespace Payment.APi.Controllers
{

    [Route("api/Vendedores")]
    public class VendedorController : MainController
    {
        private readonly IVendedorRepository _vendedorRepository;
        private readonly IVendedorService _vendedorService;
        IMapper _mapper;

        public VendedorController(IVendedorRepository vendedorRepository, IVendedorService vendedorService, IMapper mapper, INotificador notificador) : base(notificador)
        {
            _vendedorRepository = vendedorRepository;
            _vendedorService = vendedorService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<VendedorViewModel>> ObterTodos()
        {
            var veiculo = _mapper.Map<IEnumerable<VendedorViewModel>>(await _vendedorRepository.ObterTodos());
            return veiculo;
        }


        [HttpGet("{id:guid}")]
        // [Authorize]
        public async Task<ActionResult<VendedorViewModel>> ObterpPorId(Guid id)
        {
            var vendedor = _mapper.Map<VendedorViewModel>(await _vendedorRepository.ObterPorId(id));

            if (vendedor == null) return NotFound();

            return vendedor;
        }


        [HttpPost]
        public async Task<ActionResult<VendedorViewModel>> Adicionar(VendedorViewModel vendedor)
        {
            if (!ModelState.IsValid) return CostumResponse(ModelState);

            await _vendedorRepository.Adicionar(_mapper.Map<Vendedor>(vendedor));

            return CostumResponse(vendedor);
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<VendedorViewModel>> Atualizar(Guid id, VendedorViewModel vendedor)
        {
            if (id != vendedor.Id)
            {
                NotificarErro("O id informado não é o mesmo que foi passado na query");
                return CostumResponse(vendedor);
            }

            if (!ModelState.IsValid) return CostumResponse(ModelState);

            await _vendedorService.Atualizar(_mapper.Map<Vendedor>(vendedor));

            return CostumResponse(vendedor);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<VendedorViewModel>> Excluir(Guid id)
        {
            var ViewModel = await _vendedorRepository.ObterPorId(id);


            if (ViewModel == null) return NotFound();

            await _vendedorRepository.Remover(id);

            return CostumResponse(ViewModel);
        }

        
    }
}

