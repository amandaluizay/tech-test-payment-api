﻿using Payment.Business.Interfaces;
using Payment.Business.Notificacoes;
using Payment.Business.Services;
using Payment.Data.Context;
using Payment.Data.Repository;

namespace Payment.APi.Configuration
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection ResolveDependencies(this IServiceCollection services)
        {
            services.AddScoped<MeuDbContext>();
            services.AddScoped<IProdutoService, ProdutoService>();
            services.AddScoped<IProdutoRepository, ProdutoRepository>();
            services.AddScoped<IVendaRepository, VendaRepository>();
            services.AddScoped<IVendaService, VendaService>();
            services.AddScoped<IVendedorRepository, VendedorRepository>();
            services.AddScoped<IVendedorService, VendedorService>();
            services.AddScoped<INotificador, Notificador>();


            return services;
        }
    }
}
